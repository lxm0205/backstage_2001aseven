export default [
    {
        path: '/',
        component: '@/pages/Index/index',
        name: '工作台', // 兼容此写法
        icon: 'DashboardOutlined',
    },
    {
        path: "/article",
        name: "文章管理",
        icon:"FormOutlined",
        routes: [
            {
                path: "/article/allarticle",
                component: "@/pages/Article/AllArticle",
                name: "所有文章"
            },
            {
                path: "/article/classification",
                component: "@/pages/Article/Classification",
                name: "分类管理"
            },
            {
                path: "/article/label",
                component: "@/pages/Article/Label",
                name: "标签管理"
            },
        ]
    },
    {
        path: '/pageManagement',
        component: '@/pages/PageManagement',
        name: '页面管理', // 兼容此写法
        icon: 'SnippetsOutlined',
    },
    {
        path: '/knowledge',
        component: '@/pages/Knowledge',
        name: '知识小册', // 兼容此写法
        icon: 'CopyOutlined',
    },
    {
        path: '/poster',
        component: '@/pages/Poster',
        name: '海报管理', // 兼容此写法
        icon: 'StarFilled',
    },
    {
        path: '/comment',
        component: '@/pages/Comment',
        name: '评论管理', // 兼容此写法
        icon: 'MessageOutlined',
    },
    {
        path: '/message',
        component: '@/pages/Message',
        name: '邮件管理', // 兼容此写法
        icon: 'MailOutlined',
    },
    {
        path: '/file',
        component: '@/pages/File',
        name: '文件管理', // 兼容此写法
        icon: 'FolderOpenOutlined',
    },
    {
        path: '/seek',
        component: '@/pages/Seek',
        name: '搜索记录', // 兼容此写法
        icon: 'SearchOutlined',
    },
    {
        path: '/visit',
        component: '@/pages/Visit',
        name: '访问记录', // 兼容此写法
        icon: 'PartitionOutlined',
    },
    {
        path: '/user',
        component: '@/pages/User',
        name: '用户管理', // 兼容此写法
        icon: 'UserOutlined',
    },
    {
        path: '/system',
        component: '@/pages/System',
        name: '系统设置', // 兼容此写法
        icon: 'MenuOutlined',
    },
    {
        path: '/login',
        component: '@/pages/Login',
        // 新页面打开
        target: '_blank',
        // 不展示顶栏
        headerRender: false,
        // 不展示页脚
        footerRender: false,
        // 不展示菜单
        menuRender: false,
        // 不展示菜单顶栏
        menuHeaderRender: false,
        // 权限配置，需要与 plugin-access 插件配合使用
        access: 'canRead',
        // 隐藏子菜单
        hideChildrenInMenu: true,
        // 隐藏自己和子菜单
        hideInMenu: true,
        // 在面包屑中隐藏
        hideInBreadcrumb: true,
        // 子项往上提，仍旧展示,
        flatMenu: true,
    },
];