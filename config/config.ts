import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes: routes,
  layout: {
    // 支持任何不需要 dom 的
    // https://procomponents.ant.design/components/layout#prolayout
    name: '八维创作平台',
    locale: true,
    layout: 'side',
    logo:"https://img2.baidu.com/it/u=11880906,3001230239&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=313"
  },
});